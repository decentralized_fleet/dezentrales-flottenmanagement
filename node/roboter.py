#!/usr/bin/env python

import rospy
from decentralized_fleet.roboter_node import RobotNode


def main():
    rospy.init_node('Roboter', anonymous=True)

    if not rospy.has_param('~start_station_id'):
        raise Exception('Ooh i have a problem where should i start..')
    start_station_id = rospy.get_param('~start_station_id')

    rospy.sleep(2)

    isAuctioneer = rospy.get_param('~master', False)
    auction_offset = rospy.get_param('~auction_offset', 0.0)
    cost_noise = rospy.get_param('~cost_noise', 0.0)

    node = RobotNode(start_station_id, isAuctioneer, auction_offset, cost_noise)

    while not rospy.is_shutdown():
        node.loop()
        print('Loop execution')
        rospy.sleep(1)


if __name__ == '__main__':
   try:
       main()
   except rospy.ROSInterruptException:
       pass
