# Dezentrales Flottenmanagement 
This project is a ros node in a decentralized cleaning task. This tasks consits of different subtasks and this subtasks are based on a grraph that is provided by another ros node (graph_package).
Normally as in the launch files of this project you start multiple robots that then will do first a auction as an initial distribution of the substasks and then while cleaning the rooms i is possiible that roboter will exchange further tasks.

### Starten eines Szeanrios und Parametrisierung
To start the simulation use the launch files unser ./launch. They will instantiate multiple instances of the agents and one graph node as basis.

As parameters you can specify in the launch files:
* graph_dir - the location of the graph in text encoding
* master - this marks an agent instance as auctioneer in the fleet
* auction_offset - this gives an agent a additional auction cost. With tthis parameter it is possible to achive non equal distributtion at the beginning.
* start_station_id - With this you can specify where an agent starts his path.

### Szenarien
Currently there are 3 different settings/launches but for each you have tthe change the graph path cause it is currently absolute.
* Testcase_Simple -> This is only one roboter and is for simple testing/debugging
* Testcase_Real_Map -> This is used also in the documentation as a standard it references also the map that was shown in the documentation
* Testcase_Unfair_Distribution -> This is to test the exchange functions cause the robots have some auction offsets so that the distribution after the auction ist heavily listed to only few agents.
 
### Debugging
Für das Debuggen des Codes sowie des Monitoring gibt es Stellen in dieser Node, die Informationen neben den eigentlichen ros.loginfo Einträgen teilen.
Die Informationen beziehen sich einerseits auf den aktuellen Kostenstand für die geschätzten und empfangenen Kosten. Diese NAchrichten laufen auf dem Channel 'Cost' mit dem Nachrichtenformat ./msg/DebugCostChange.msg.
Die zweite Information, die veröffentlich wird ist ist die beanspruchte Zeit des Brute Force und des GA, sowie die zugehörigen geschätzten Kosten der beiden optimierten Pfade. Damit man dise gut vergleichen kann werden immer beide optimierungen ausgeführt. Sollte dies nicht gewünscht sein müsste der Aufruf in der Datei ./path.py in der MEthode optimize auskommeniert werden. Die NAchrichten werden auf dem Channel 'Debug'  mit dem NAchrichtenformat ./masg/PathOptimizationTarget.msg.