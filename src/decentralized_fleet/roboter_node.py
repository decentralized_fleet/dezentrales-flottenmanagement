#!/usr/bin/env python
from enum import Enum
class RobotState(Enum):
    AUCTION = 1
    EXCHANGE = 2
    WORKING = 3
    DONE = 4

from decentralized_fleet.auction_subscriber import AuctionSubscriber
from decentralized_fleet.path import Path
from .path import Path
from decentralized_fleet.auctioneer import Auctioneer
from decentralized_fleet.auction_subscriber import AuctionSubscriber
from decentralized_fleet_graph.srv import GetAllRoomIds

from decentralized_fleet.msg import AuctionInfo, ExchangeInfo, DebugCostChange
from .exchange import Exchanger
import rospy
import time, random




class RobotNode:
    """
    This is the main class for an agent. Each instant of this class represents an agent that is able to progress by calling the loop function.

    Args:
        start_station_id (str): The identifier for the station that is marked as the starting point for this agent
        is_Auctioneer (bool): This indicates if this agent is the auctioneer in the beginning auction.
        auction_offset (double): With this you can apply a disadvantage to this agent instance. Cause in the auction the agent will always add on the current costs this offset. With this parameter you are able to test different settings.
        cost_noise (double): The sigma to be applied when introducting artifical uncertainty in path cost using a normal distribution.

    Attributes:
        state (RobotState): This is the internal state of the agent.
        path (Path): This is the current path of the agent. It contains all the subtasks the agent committed himself to and the order of them.
        is_Auctioneer (bool): This indicates if this agent is the auctioneer in the beginning auction.
        auction_offset (double): With this you can apply a disadvantage to this agent instance. Cause in the auction the agent will always add on the current costs this offset. With this parameter you are able to test different settings.
        cost_noise (double): Sigma to be applied when introducting artifical uncertainty in path cost using a normal distribution.
        open_and_not_answered_task_ids (List<string>): This is a list of all received auction offers that are currently not answered by the robot. 
        auction_publisher (Publisher): This is the publisher for the Auction Topic 
        auction_subscriber (AuctionSubscriber) : This instance is the Auction Subscriber. In this instance the messages of the Auction Topic will arrive and get categorized
        exchanger (Exchanger): This is the exchanger instance that will coordinate all running exchanges processes
        my_commited_bids (List<ExchangeInfo>): This are all exchange_infos this node sended to know which one to update if the path updates in the meanwhile
        my_exchange_offer (ExchangeInfo): If the node offers a task in the exchange state it saves the ExchangeInfo here
        my_exchange_offer_duration (int): How many active exchange iterattions my current offer is online.
        not_success_cost (double): With this the agent remeber the potential improvement of his last not successfull exchange. This should be an estimation that his next task he offers should be less valuable to get a chance of a successfull exchange.
        my_received_exchange_bids (List<ExchangeInfo>): Here all responses to the exchange_offer of this node are saved.
        not_answered_exchange_infos (List<ExchangeInfo>) This will contain all offers from other robots.  And the robot can then decide if it wants to answer it.
        finished_task_at_time (int): This is the time the roboter finishes it current cleaning subtask. When the roboter is currently cleaning a subtasks this time will be in the future. 
        remaining_exchange_iterations (int): When the roboter is in the state acitvely take part on the exchange process, this member will indicate the remaninng interations the riboter has in this state before changing back to the passive cleaning part
        last_send_cost (double): The roboter saves the cost with which it gave the debug node the last update to not send a mess of updates.
        auctioneer (Auctioneer): When this node is the auctioneer of the current session in this memeber iis a instance of Auctioneer stored.
        debug_cost_publisher (Publisher): This is the publisher for the debug node to get a insight to the way how the simulation is working.


    """
    def __init__(self, start_station_id, is_Auctioneer, auction_offset, cost_noise):
        self.state = RobotState.AUCTION
        self.path = Path()
        self.path.add_task_to_path_and_optimize(start_station_id)

        self.is_Auctioneer = is_Auctioneer
        self.auction_offset = auction_offset
        self.cost_noise = cost_noise

        # This is a list where the auction subscriber can save all Auction_start messages.
        self.open_and_not_answered_task_ids = []
        self.auction_publisher = rospy.Publisher('Auction', AuctionInfo, queue_size=10)
        self.auction_subscriber = AuctionSubscriber(self)

        self.exchanger = Exchanger(self)
        self.my_commited_bids = [] # This are all exchange_infos this node sended to know which one to update if the path updates in the meanwhile
        self.my_exchange_offer = None  # If the node offers a task in the exchange state it saves the ExchangeInfo here
        self.my_exchange_offer_duration = 0
        self.not_success_cost = 0
        self.my_received_exchange_bids = [] # Here all responses to the exchange_offer of this node are saved.
        self.not_answered_exchange_infos = [] # This will contain all offers from other robots.  And the robot can then decide if it wants to answer it.
        self.finished_task_at_time = 0
        self.remaining_exchange_iterations = 0

        self.last_send_cost = .0

        if is_Auctioneer:
            self.auctioneer = Auctioneer()

        self.debug_cost_publisher = rospy.Publisher('Cost',DebugCostChange,queue_size=10)

    def loop(self):
        '''
        This method is the main method of the agent. It will look on the internal state of the agent and will then execute the steps for this state. 
        '''
        if self.state == RobotState.AUCTION:
            self.do_auction_actions()
        elif self.state == RobotState.EXCHANGE:
            self.exchanger.answer_open_exchange_offers()
            self.do_exchange_actions()
            self.remaining_exchange_iterations -= 1

            if self.remaining_exchange_iterations % 5 == 0:
                self.reset_exchange()

            if self.remaining_exchange_iterations <= 0:
                self.execute_next_task()
        elif self.state == RobotState.WORKING:
            self.exchanger.answer_open_exchange_offers()

            if time.time() > self.finished_task_at_time:
                if self.path.current_position + 1 >= len(self.path.path):
                    self.state = RobotState.DONE
                    rospy.loginfo('I ended my path {} with {} time.'.format(self.path.path, self.path.current_objective_time_used))
                else:
                    self.state = RobotState.EXCHANGE
                    self.remaining_exchange_iterations = 10
        elif self.state == RobotState.DONE:
            self.exchanger.answer_open_exchange_offers()

            if self.path.is_there_something_to_do():
                self.execute_next_task()

        if self.state != RobotState.DONE:
            self.debug_cost_publish()

    def debug_cost_publish(self, event = ''):
        """
        For debugging this method send a message to the debug channel 'Cost' whenever the cost are changed.
        """
        cost = self.path.get_objective_path_cost()
        if cost != self.last_send_cost:
            self.debug_cost_publisher.publish(DebugCostChange(RoboterId=rospy.get_caller_id(), Time=time.time()%3000,
                                                          Cost=cost,UsedTime=self.path.current_objective_time_used,
                                                              Event=event))

    def execute_next_task(self):
        """
        This method will be called as a transistion from the active exchange oart to the passive one.
        The transistion starts with cancelling the current own bid of the agent.
        After rthat it evaluates what the next step will be according to the path.
        In the case the agent is on the end of its path the agent will go to the state DONE.
        If not the agent goes to the state WORKING and will before calculate the objective time it has now to wait idle.
        But before waiting this time there is some noise getting added to this time to simulate that the path costs are only estimations.
        """
        self.reset_exchange()

        if not self.path.is_there_something_to_do():
            self.state = RobotState.DONE
            rospy.loginfo(
                'I ended my path {} with {} time.'.format(self.path.path, self.path.current_objective_time_used))
            return

        self.state = RobotState.WORKING
        objective_time_cost_for_next_step = self.path.get_objective_cost_for_next_step(self.cost_noise)
        self.finished_task_at_time = time.time() + objective_time_cost_for_next_step
        self.path.done_next_task(objective_time_cost_for_next_step)
        rospy.loginfo('I will work on task {} current cost: {}'.format(self.path.path[self.path.current_position],
                                                                       self.path.get_objective_path_cost()))

    def reset_exchange(self):
        if self.my_exchange_offer is not None:
            self.not_success_cost = self.my_exchange_offer.OldObjectiveCost - self.my_exchange_offer.NewObjectiveCost

        self.my_received_exchange_bids = []
        self.my_exchange_offer = None
        self.my_exchange_offer_duration = 0


    def do_auction_actions(self):
        self.answer_open_auctions()

        if self.is_Auctioneer:
            self.auctioneer.step()


    def do_exchange_actions(self):
        self.exchanger.step()



    def answer_open_auctions(self):
        """
        This method take a look in the list with the not answered auctions offers and iterate over all of this open offers.
        For each offer the agent will evaluate its cost and will create a bid for the subtask. This offer is then send in the Auction Channel.
        """
        while len(self.open_and_not_answered_task_ids) > 0:

            potential_task = self.open_and_not_answered_task_ids.pop()
            potential_path = self.path.get_new_optimized_path_with_additional_task(potential_task)
            new_objective_cost = potential_path.get_objective_path_cost() + self.auction_offset
            self.auction_publisher.publish(AuctionInfo(Method='BID', Source=rospy.get_caller_id(), TaskId=potential_task, Amount=-new_objective_cost))

    def end_auction(self):
        self.state = RobotState.EXCHANGE
