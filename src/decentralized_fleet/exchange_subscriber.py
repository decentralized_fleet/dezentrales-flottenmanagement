import rospy
from decentralized_fleet.msg import ExchangeInfo


class ExchangeSubscriber:

    """
    This class coordinates all the incomming messages from the exchanges process. For that this instance take alle incomming messages and classify them. In a second step the classified messages are added to internal lists of the robot. So the robot is able to work on them on the next time it will have an active cycle.

    Args:
    roboter_node (roboter_node): The reference to the parent roboter_node to have access to all the internal lists
    """

    def __init__(self, roboter_node):
        self.subscriber = rospy.Subscriber('Exchange', ExchangeInfo, self.callback)
        self.roboter_node = roboter_node

    def callback(self, exchange_info):
        if exchange_info.Method == 'OFFER':
            """An incomming Offer will be validated to be from another robot and in the second step to a waiting queue."""
            if exchange_info.Source == rospy.get_caller_id():
                # We don't want to exchange with our self
                return
            self.roboter_node.not_answered_exchange_infos.append(exchange_info)
        elif exchange_info.Method == 'BID' or exchange_info.Method == 'Reset':
            """
            All incomming bids to subtaks will be processed here.
            The first step is to validate that the bid is targeted to the current exchange offer of the according robot.
            Afterthat it will be checked of the bid has to be created/updated or removed from the internal memory.
            """
            if self.roboter_node.my_exchange_offer is None or exchange_info.TaskId != self.roboter_node.my_exchange_offer.TaskId:
                return

            existing_bids_from_this_source = [bid for bid in self.roboter_node.my_received_exchange_bids if
                                              bid.Source == exchange_info.Source]
            if any(existing_bids_from_this_source):
                self.roboter_node.my_received_exchange_bids.remove(existing_bids_from_this_source[0])
            if exchange_info.Method != 'RESET':
                self.roboter_node.my_received_exchange_bids.append(exchange_info)

        elif exchange_info.Method == 'ASSIGNMENT':
            """
            Whenever a ASSIGNEMT comes into the method it will be validated that this messages belongs to the robot.
            After that the subtasks will be assigned to the path of the robot.
            And because due to the assignment hte cost changed all current open bids has to be reevaluteed and update all bids.
            This habbens by setting a flag in the roboter_node and then the active exchange instance will update all bids.
            """

            my_bid = next((bid for bid in self.roboter_node.my_commited_bids if bid.TaskId == exchange_info.TaskId), None)
            if my_bid is None:
                return

            rospy   .loginfo('Before delete {}'.format(self.roboter_node.my_commited_bids))
            self.roboter_node.my_commited_bids.remove(my_bid)
            rospy.loginfo('After delete {}'.format(self.roboter_node.my_commited_bids))

            if exchange_info.Source != rospy.get_caller_id():
                return
            self.roboter_node.debug_cost_publish('Tausch {} angenommen'.format(exchange_info.TaskId))
            rospy.loginfo('{} got {} my new path {} current bids {}'.format(rospy.get_caller_id(), exchange_info.TaskId,
                                                            self.roboter_node.path.path, self.roboter_node.my_commited_bids))
            self.roboter_node.path.add_task_to_path_and_optimize(exchange_info.TaskId)
            self.roboter_node.exchanger.need_update_bids = True

