import rospy
import time, random
import numpy as np
from decentralized_fleet.msg import AuctionInfo
from decentralized_fleet_graph.srv import GetAllRoomIds
from decentralized_fleet.path import get_objective_time_from_room_cost, get_subjective_cost_from_room


class Auctioneer:
    """
    This class is the active part of an auction. Only one agent of the fleet can have an instance of this class.
    Its task is to start an auction for every task and find for each task an agent that gets this task assigned.
    It saves locally which tasks are assigned, which is the rreason that there cann not be more then one auctioneer.

    Args:
    auctioneer_publisher (Publisher): The ROS publisher to publish message on the Auction channel.
    auctioneer_subscriber (subscriber): The ROS subscriber to receive message on the Auction channel.
    active_auction_room (str): The current room that is active in the auction
    active_auction_start (long): This contains the ticks of the time when the current auctions has started to know when to end the auchtion.
    auction_history (Dictionary): Here this instance saves all best bids of as a 1 to 1 relationship to the subtasks that were part of the auction
    all_rooms (List): At the beginning the auctioneer fills the list with all possiible room Ids that are existing in the graph
    active (Bool): Indicates of the auctioneer is done with its task.
    """
    def __init__(self):
        self.auctioneer_publisher = rospy.Publisher('Auction', AuctionInfo, queue_size=10)
        self.auctioneer_subscriber = rospy.Subscriber('Auction', AuctionInfo, self.callback)
        self.active_auction_room = ''
        self.active_auction_start = 0
        self.auction_history = {}
        self.all_rooms = []
        self.active = True

    def callback(self, auction_info):
        """
        This method takes tthe messages of the AUction Channel and look only for the bids of the aoher agents.
        Then is validated if this Bids belonging to the current auction and if so itt will checkl if this is a better bid then the current and save it locally.

        Args:
        auction_info (Auction_Info): The auction mesage.
        """
        if auction_info.Method == 'BID':
            if auction_info.TaskId != self.active_auction_room:
                return

            if self.auction_history.has_key(auction_info.TaskId) and auction_info.Amount <= self.auction_history[
                auction_info.TaskId].Amount and self.auction_history[auction_info.TaskId].Source != auction_info.Source:
                return

            self.auction_history[auction_info.TaskId] = auction_info


    def step(self):
        """
        This method is a active step of the auctioneer.
        In one step this instance either starrts a new auction with a not assigned room or it end a auction when the auction was long enough active (5 sec).
        """
        if not self.active:
            return

        if len(self.all_rooms) == 0:
            rospy.wait_for_service('getAllRoomIds')
            get_all_room_ids = rospy.ServiceProxy('getAllRoomIds', GetAllRoomIds)
            self.all_rooms = get_all_room_ids().RoomIds

        if len(self.active_auction_room) < 1:
            # Start a new auction
            free_room_ids = [room for room in self.all_rooms if room not in self.auction_history]

            if len(free_room_ids) == 0: # Auction over
                self.auctioneer_publisher.publish(AuctionInfo(Method='AUCTION_OVER'))
                self.active = False
                return

            self.active_auction_room = sorted(free_room_ids, key=lambda room: get_objective_time_from_room_cost(get_subjective_cost_from_room(room)), reverse=True)[0]

            self.active_auction_start = time.time()
            auction_info = AuctionInfo(Method='AUCTION_START', TaskId=self.active_auction_room, Source=rospy.get_caller_id())
            self.auctioneer_publisher.publish(auction_info)
        else:
            if time.time() - self.active_auction_start > 5:
                if self.active_auction_room in self.auction_history:
                    msg = self.auction_history[self.active_auction_room]
                    self.active_auction_room = ''
                    self.active_auction_start = 0
                    msg.Method = 'HIGHEST_BID'
                    self.auctioneer_publisher.publish(msg)
