import rospy
import numpy as np
from decentralized_fleet_graph.srv import GetMapCostForRoom, GetMapCostForPath
from decentralized_fleet.path_optimization_ga import optimize_ga_part_with_start
from decentralized_fleet.msg import PathOptimizationTarget
import time

class Path:
    """
    This class represents the path of an agent. The class is a wrapper around the list of subtakss with some more functionality for the agent.

    Attributes:
    path (List<string>): The content of the path with all subtasks in the according order
    current_position (int): This is the index where the robot is currently located. The subtasks where the robot is currently located is means that this subtask is done.
    current_objective_time_used (double) : Cause the costs of the future subtasks are only estimations, in this variable all real costs are sumed up including the position of the robot
    
    
    """
    def __init__(self):
        self.path = []
        self.current_position = 0
        self.current_objective_time_used = 0
        self.debug_publisher = rospy.Publisher('Debug', PathOptimizationTarget, queue_size=10)

    def get_new_optimized_path_with_additional_task(self, task_id):
        new_path = self.get_cloned_path()
        new_path.add_task_to_path_and_optimize(task_id)
        return new_path

    def get_cloned_path(self):
        """Creates a new path based on the currentt instance"""
        new_path = Path()
        new_path.path = [task for task in self.path]
        new_path.current_position = self.current_position
        new_path.current_objective_time_used = self.current_objective_time_used
        return new_path
    
    def optimize(self):
        """OPtimizes the current path. For that currenttly bot optimizations methods are triggered. But only the result of the Brute Force is used. After everything is calculated the results are published to the debug node."""

        cache = {}
        

        start = time.time()
        path_ga = self.path[:self.current_position + 1] + optimize_ga_part_with_start(
            self.path[self.current_position + 1:], get_objective_cost_for_path)
        ga_duration = time.time() - start
        start = time.time()
        self.path = self.path[:self.current_position + 1] + optimize_with_start_position(
            self.path[self.current_position], self.path[self.current_position + 1:], cache)
        duration = time.time()-start

        self.debug_publisher.publish(PathOptimizationTarget(
            length=len(self.path[self.current_position + 1:]),
            valueConventional = self.get_objective_path_cost(),
            valueGenetic =  get_objective_cost_for_path(path_ga, self.current_position) + self.current_objective_time_used,
            timeConventional = duration,
            timeGenetic = ga_duration
        ))


    def get_objective_path_cost(self):
        return get_objective_cost_for_path(self.path, self.current_position) + self.current_objective_time_used

    def done_next_task(self, time_used):
        """
        This method is called whenever a task is completed and the robot goes back to the active exchange part.

        Args:
        time_used (double): The real time the robot has used for the way to the last subtasks and the time for doing the subtask.
        """
        self.current_position += 1
        self.current_objective_time_used += time_used

    def add_task_to_path_and_optimize(self, task_id):
        self.path.append(task_id)
        if self.current_position < len(self.path) - 1:
            self.optimize()

    def get_objective_cost_for_next_step(self, sigma):
        """
        For the next step after the current position this method caluclates the time it needs.
        This method does not rreturn the estimation this are the real cost. To be more realtistic is is possible to enable some noise on this time.

        Args:
        sigma (double): Sigma to be used for the normal distribution when applying a noise to the cost.
        """

        time_since_next_event = get_objective_time_from_path_cost(
            get_subjective_cost_from_path(self.path[self.current_position], self.path[self.current_position + 1]))
        subjective_task_cost = get_subjective_cost_from_room(self.path[self.current_position + 1])

        time_since_next_event += get_objective_time_from_room_cost(subjective_task_cost)
        if sigma > 0.0:
            time_since_next_event = add_noise_to_objective_room_cost(time_since_next_event, sigma)

        return time_since_next_event

    def get_worst_task_and_objetive_cost(self, not_successful_exchanging_cost):
        """
        This method returns that subtask that has the biggest impact on the costs of the agent. This is constrained with a maximum impact that is not overexaggreated.

        Args:
        not_successful_exchanging_cost (double): This is the limit impact for the worst task
        """

        task, old_objective_cost, new_objective_cost = calculate_worst_task_and_objective_costs(self, not_successful_exchanging_cost)
        return {'TaskId': task, 'OldObjectiveCost': old_objective_cost, 'NewObjectiveCost': new_objective_cost}

    def is_there_something_to_do(self):
        """Is the robot already done with all its tasks?"""
        return self.current_position < len(self.path) - 1


def calculate_worst_task_and_objective_costs(path, not_successful_exchanging_cost):
    """
    This method returns that subtask of the path that has the biggest impact on the costs of the agent. This is constrained with a maximum impact that is not overexaggreated.

    Args:
    path: The path from that the worst subtask gets extracted
    not_successful_exchanging_cost (double): This is the limit impact for the worst task
    """
    old_objective_cost = path.get_objective_path_cost()

    min_cost = old_objective_cost
    deleted_task = ''

    for task in path.path[path.current_position+1:]:
        cloned_path = path.get_cloned_path()
        cloned_path.path.remove(task)
        cloned_path.optimize()
        optimal_objective_cost = cloned_path.get_objective_path_cost()

        if optimal_objective_cost < min_cost and (not_successful_exchanging_cost == .0 or get_objective_time_from_room_cost(get_subjective_cost_from_room(task)) < not_successful_exchanging_cost):
            min_cost = optimal_objective_cost
            deleted_task = task

    rospy.loginfo('Delete the task {}'.format(deleted_task))
    return deleted_task, old_objective_cost, min_cost


def optimize_with_start_position(start_position_id, path_to_optimize, cache=None):
    """
    This method optimizes a given path from a start position index.

    Args:
    start_position_id (int): This is the start position from where the path get optimized. The subtask at the index itself does not get optimized.
    path_to_optimize (LIst<string>): The path to optimize
    cache ({}): This cache can remeber the subject cost of rooms and the ways between the rooms to not need a service call for every cost query.
    """
    if len(path_to_optimize) <= 1:
        return path_to_optimize

    min_cost = float('inf')
    min_cost_path = []

    for index in range(len(path_to_optimize)):
        opt_path = optimize_with_start_position(path_to_optimize[index],
                                                     path_to_optimize[:index] + path_to_optimize[index + 1:],
                                                     cache)
        cost = get_objective_cost_for_path([start_position_id] + [path_to_optimize[index]] + opt_path, 0, cache)
        if cost < min_cost:
            min_cost = cost
            min_cost_path = [path_to_optimize[index]] + opt_path

    return min_cost_path

def get_objective_cost_for_path(path, current_position, cache = None):
    cost = 0

    if current_position >= len(path):
        return cost

    position_id = path[current_position]
    for room_id in path[current_position+1:]:
        cost += get_objective_time_from_path_cost(get_subjective_cost_from_path(position_id, room_id, cache))
        cost += get_objective_time_from_room_cost(get_subjective_cost_from_room(room_id, cache))
        position_id = room_id
    return cost

def add_noise_to_objective_room_cost(objective_room_time_cost, sigma):
    noise_fac = -1
    while noise_fac <= 0.0:
        noise_fac = np.random.normal(1, sigma)
    return objective_room_time_cost * noise_fac

def get_objective_time_from_path_cost(path_cost):
    return path_cost[0] / 50

def get_objective_time_from_room_cost(room_cost):
    return room_cost[0] / 50

service_call_count = 0

def get_subjective_cost_from_room(element_id, cache = None):
    """
    This method does a service call to the graph node to get for a room the mapcosts.
    Afterward the mapcosts can also be modified.

    Args:
    element_id (string): The room id.
    cache ({}): This cache can remeber the subject cost of rooms and the ways between the rooms to not need a service call for every cost query.
    """
    global service_call_count

    if cache is not None and element_id in cache:
        map_cost = np.array(cache[element_id])
    else:
        try:
            rospy.wait_for_service('getMapCostForRoom')
            get_map_cost_for_path = rospy.ServiceProxy('getMapCostForRoom', GetMapCostForRoom)
            service_response = get_map_cost_for_path(element_id)
            map_cost = list(service_response.Cost)
            service_call_count += 1
            if cache is not None:
                cache[element_id] = map_cost

            map_cost = np.array(map_cost)
        except:
            rospy.loginfo('Failed {} when calculating path {}'.format(rospy.get_caller_id(),element_id))



    # Modify them to get subjective cost
    map_cost[0] = map_cost[0] * 45

    return map_cost

def get_subjective_cost_from_path(pointA, pointB, cache = None):
    """
    This method does a service call to the graph node to get for a path bewtween two rooms the mapcosts.
    Afterward the mapcosts can also be modified.

    Args:
    pointA (string): The origin of the path
    pointB (string): The target of the path
    cache ({}): This cache can remeber the subject cost of rooms and the ways between the rooms to not need a service call for every cost query.
    """
    global service_call_count
    path_cache_id = '{}<->{}'.format(pointA,pointB)
    if cache is not None and path_cache_id in cache:
        map_cost = np.array(cache[path_cache_id])
    else:
        try:
            rospy.wait_for_service('getMapCostForPath')
            get_map_cost_for_path = rospy.ServiceProxy('getMapCostForPath', GetMapCostForPath)
            map_cost = list(get_map_cost_for_path(pointA, pointB).Cost)
            service_call_count += 1
            if cache is not None:
                cache[path_cache_id] = map_cost
            map_cost = np.array(map_cost)
        except:
            rospy.loginfo('Failed {} when calculating path {}'.format(rospy.get_caller_id(),path_cache_id))

    # Modify them to get subjective cost
    map_cost[0] = map_cost[0] / 100 * 7

    return map_cost
