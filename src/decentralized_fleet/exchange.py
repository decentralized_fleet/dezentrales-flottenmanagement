import rospy
from decentralized_fleet.msg import ExchangeInfo
from decentralized_fleet.exchange_subscriber import ExchangeSubscriber

class Exchanger:
    """
    This class represents the active part of the exchange state. In this instance tha agent will consider the exchange offers and send responses to the other agents. To remember all the incomming offers and bids from the exchange_subscriber the agent works on internal lists.

    Args:
    robot_noder (roboter_node): This is the agent itself you need a reference to access the internal lists.

    Attributes:
    exchange_publisher: This is the ROS publisher instance that is wrapped by this class
    robot_node: The parent agent from the parameters
    subscriber (ExchangeSubscriber): The exchange subscriber to receive the messages regarding exchange processes 
    need_update_bids (Boolean): This is a flag inside the class that is evaluated in each call of the step method to signal the instance that it has to evaluate if the current bids are up to date.
    """

    def __init__(self, robot_node):
        self.exchange_publisher = rospy.Publisher('Exchange', ExchangeInfo, queue_size=10)
        self.robot_node = robot_node
        self.subscriber = ExchangeSubscriber(self.robot_node)
        self.need_update_bids = False

    def step(self):
        """
        This method should be called in each step when the agent is in the state EXCHANGE. 
        The method will when the agent is the source of an current open exchange look for a good received bid. When there is a good bid it will finish the exchange. If there is no current bid this method will look for the next subtask to offer to the other agents and will publish this offer.
        """
        if self.robot_node.my_exchange_offer is not None:
            if len(self.robot_node.my_received_exchange_bids) > 0:
                best_bid = sorted(self.robot_node.my_received_exchange_bids, key=lambda ex_info: ex_info.NewObjectiveCost)[0]
                if best_bid.NewObjectiveCost < self.robot_node.my_exchange_offer.OldObjectiveCost:
                    best_bid.Method = 'ASSIGNMENT'
                    self.exchange_publisher.publish(best_bid)

                    self.robot_node.path.path.remove(self.robot_node.my_exchange_offer.TaskId)
                    self.robot_node.path.optimize()

                    self.robot_node.my_exchange_offer = None
                    self.robot_node.my_exchange_offer_duration = 0
                    self.robot_node.not_success_cost = .0
                    self.robot_node.my_received_exchange_bids = []
                    self.update_my_exchange_bids()
                    rospy.loginfo('{} assigned {} to {}'.format(rospy.get_caller_id(), best_bid.TaskId, best_bid.Source))
                    self.robot_node.debug_cost_publish('Tausch {} abgegeben'.format(best_bid.TaskId))
        else:
            worst = self.robot_node.path.get_worst_task_and_objetive_cost(self.robot_node.not_success_cost)
            if worst['TaskId'] != '':
                self.robot_node.my_exchange_offer = ExchangeInfo(Method='OFFER', TaskId='{}'.format(worst['TaskId']), Source=rospy.get_caller_id(), OldObjectiveCost= worst['OldObjectiveCost'], NewObjectiveCost=worst['NewObjectiveCost'])
                self.exchange_publisher.publish(self.robot_node.my_exchange_offer)
                rospy.loginfo('{} offered {}'.format(rospy.get_caller_id(), self.robot_node.my_exchange_offer))


    def update_my_exchange_bids(self):
        """
        This method is to update the own bids that are open. It checks if the bid amount is up to date. Should it not be up to date it will send a new BID message or when it is not efficient anymore it will take the BID back.
        """
        for exchange_info in self.robot_node.my_commited_bids:
            rospy.loginfo('{} update {}'.format(rospy.get_caller_id(), exchange_info.TaskId))
            current_objective_cost = self.robot_node.path.get_objective_path_cost()
            new_objective_cost = self.robot_node.path.get_new_optimized_path_with_additional_task(exchange_info.TaskId).get_objective_path_cost()

            if new_objective_cost < exchange_info.OldObjectiveCost:
                ex_info = ExchangeInfo(Method='BID', TaskId=exchange_info.TaskId, Source=rospy.get_caller_id(),
                                       OldObjectiveCost=current_objective_cost,
                                       NewObjectiveCost=new_objective_cost)
                self.exchange_publisher.publish(ex_info)
                self.robot_node.my_commited_bids.remove(exchange_info)
                self.robot_node.my_commited_bids.append(ex_info)
            else:
                self.exchange_publisher.publish(ExchangeInfo(Method='RESET', TaskId=exchange_info.TaskId, Source=rospy.get_caller_id()))
                self.robot_node.my_commited_bids.remove(exchange_info)

    def answer_open_exchange_offers(self):
        """
        This method looks in the internal lists of the agent and evaluate all the open requests.
        When the exchange would be efficient it sends a bid method with the Publisher.
        """

        if self.need_update_bids:
            self.update_my_exchange_bids()
            self.need_update_bids = False

        while len(self.robot_node.not_answered_exchange_infos) > 0:
            exchange_info = self.robot_node.not_answered_exchange_infos.pop()

            if any((task for task in self.robot_node.my_commited_bids if task.TaskId == exchange_info.TaskId)):
                # It is important to clear all old offers of this task.They can be sttill in this list because maybe there was no successfull ack and the other node tries this exchange again.
                self.robot_node.my_commited_bids = [task for task in self.robot_node.my_commited_bids if task.TaskId != exchange_info.TaskId]

            rospy.loginfo('Calculate')
            current_objective_cost = self.robot_node.path.get_objective_path_cost()
            new_path = self.robot_node.path.get_new_optimized_path_with_additional_task(exchange_info.TaskId)
            new_objective_cost = new_path.get_objective_path_cost()

            rospy.loginfo('Cost comparison: {} to {}, compared old from other {}, the new path {}'.format(current_objective_cost,new_objective_cost, exchange_info.OldObjectiveCost,new_path.path))

            if new_objective_cost < exchange_info.OldObjectiveCost:
                ex_info = ExchangeInfo(Method='BID', TaskId=exchange_info.TaskId, Source=rospy.get_caller_id(),
                                       OldObjectiveCost=current_objective_cost,
                                       NewObjectiveCost=new_objective_cost)
                self.exchange_publisher.publish(ex_info)
                rospy.loginfo(ex_info)
                self.robot_node.my_commited_bids.append(exchange_info)
