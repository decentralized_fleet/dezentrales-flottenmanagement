import rospy
from decentralized_fleet.msg import AuctionInfo


class AuctionSubscriber:
    """
    The ttask of this class is to take part on a organzied auction. 
    The class will receive auction offers and will evaluate what the amount of a bid would be. This amount is send wwrapped in a bid message to the auctioneer.

    Args:
    roboter_node: The parent agent to be able to access the path variable to optimize the path with the new potential subtask.

    Attribute:
    subscriber (Subscriber): A ROS Subscriber instance to listen for all auction messages. 
    """
    def __init__(self, roboter_node):
        self.subscriber = rospy.Subscriber('Auction', AuctionInfo, self.callback)
        self.roboter_node = roboter_node

    def callback(self, auction_info):
        """
        This method is the callback of the internal ROS subsciber and its task is to take the different messages and proccess them.

        Args:
        auction_info: The message that comnes from the ROS subscriber.
        """
        if auction_info.Method == 'AUCTION_START':
            self.roboter_node.open_and_not_answered_task_ids.append(auction_info.TaskId)
        elif auction_info.Method == 'HIGHEST_BID':
            if auction_info.Source == rospy.get_caller_id():
                self.roboter_node.path.add_task_to_path_and_optimize(auction_info.TaskId)
                rospy.loginfo('I {} got a new task {} my path {}'.format(rospy.get_caller_id(), auction_info.TaskId, self.roboter_node.path.path))
                self.roboter_node.debug_cost_publish('Auktion {} bekommen'.format(auction_info.TaskId))
        elif auction_info.Method == 'AUCTION_OVER':
            self.roboter_node.end_auction()
            rospy.loginfo('Auction over with path {} and predeicted cost: {}'.format(self.roboter_node.path.path, self.roboter_node.path.get_objective_path_cost()))
