from pyeasyga import pyeasyga
import numpy as np
import rospy


cost_callback = None
cache =   {}

def optimize_ga_part_with_start(path, cost_cb):
    """
    This method optimizes a given path regarding to the passed cost function with the genetic algorithm.

    Args:
    path (List<string>): The srings of the subtasks that should get optimized
    cost_cb (Callback): This is a function that should take ttwo ids of the subtasks and return a double that indicastes the cost.

    Returns:
    This method returns a path tthat is based on the passed subtask ids and is optimized by the GA.

    """
    global cost_callback,cache

    if len(path) < 2:
        return path

    cache = {}
    cost_callback = cost_cb
    ga = pyeasyga.GeneticAlgorithm(path, population_size = 15, generations = 50)
    ga.create_individual = create_individual
    ga.crossover_function = crossover
    ga.mutate_function = mutate
    ga.fitness_function = fitness
    ga.run()
    return list(ga.best_individual()[1])


def create_individual(data):
    """
    With this method it is described how individuals (solutions) can be created.
    Special to this usecase is that a solution cannot have the same element twice.
    So only permutations of the initial path can be solutions.
    """
    individual = np.array(data[:])
    np.random.shuffle(individual)
    return individual

def crossover(parent1, parent2):
    """
    This method os the used crossover. The ask is to take two solutions and mix them up in a way to find new good solutions structures but hold good structures.
    This crossover takes a subsequence of one parent solutions and substract them of the second parent but hold the order of the rest. Now you habe two distinct subsequences that can be concatenated in two ways. This two are then the return of this function.
    """
    geneA = int(np.random.random() * len(parent1))
    geneB = int(np.random.random() * len(parent1))

    startGene = min(geneA, geneB)
    endGene = max(geneA, geneB)

    childP1 = []
    for i in range(startGene,endGene):
        childP1.append(parent1[i])

    childP2 = [gene for gene in parent2 if gene not in childP1]
    return [childP1 + childP2, childP2 + childP1]

def mutate(individual):
    """
    The mutation functions target is to take tow parent genes and bring some randomness into the solutions.
    This is reached with changing two subtasks in each solution oin random positions.
    """
    mutate_index1 = int(np.random.random() * len(individual))
    mutate_index2 = int(np.random.random() * len(individual))

    gene1 = individual[mutate_index1]
    gene2 = individual[mutate_index2]

    individual[mutate_index2] = gene1
    individual[mutate_index1] = gene2

def fitness(individual, data):
    """
    The fitness value is the estimated cost of this oath/solution.
    It is negative cause in the used libaray for the GA, the bigger the the fitness the better is the solution.
    """
    return -cost_callback(individual, 0,cache)
